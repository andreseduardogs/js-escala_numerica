![Escala](http://www.madarme.co/portada-web.png)
#  Titulo del proyecto: escala numerica

En este proyecto se realiza un algoritmo que hace el calculo de una escala numerica deacuerdo a la de un numero que se proporcione

# Tabla de contenido
1. [Esala Numerica](#titulo-del-proyecto-escala-numerica)
2. [Ejemplo](#ejemplo)
3. [Caracteristicas]
4. [Tecnologias]
5. [IDE]
6. [Demo]
7. [Contribución]
8. [Autor]
9. [Institución Educativa]

## Ejemplo
Acontinuación ejemplo del funcionamiento de una Escala Numerica. Se muestra tabla con datos referentes Escala del numero: 1

> Operacion multiplicar

| dato 1 | dato 2 | Resultado |
|--------|--------|-----------|
| 1      | 1      | 1         |
| 1      | 2      | 2         |
| 2      | 3      | 6         |
| 6      | 4      | 24        |
| 24     | 5      | 120       |
| 120    | 6      | 720       |
| 720    | 7      | 5040      |
| 5040   | 8      | 40320     |
| 40320  | 9      | 362880    |
| 362880 | 10     | 3628800   |

## Caracteristicas

